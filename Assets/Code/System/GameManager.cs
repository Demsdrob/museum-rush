using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    [SerializeField] private int _level;
    private MemoryScript _memoryScript;
    private AudioSource _source;
    private Controls _inputActions;

    private void Awake() => _inputActions = new Controls();
    private void OnEnable() => _inputActions.Enable();
    private void OnDisable() => _inputActions.Disable();
    private void Start()
    {
        _memoryScript = MemoryScript.memory;
        _source = _memoryScript.GetComponent<AudioSource>();
        _inputActions.Game.Restart.performed += ctx => RestartLevel();
    }

    public void UnlockPage()
    {
        _memoryScript?.UnlockPage(_level - 1);
        _memoryScript?.Save();
    }
    public void Mute(bool value)
    {
        if (_source == null) return;
        _source.mute = value;
    }

    public void QuitGame() => Application.Quit();
    public void SetScene(string scene) => SceneManager.LoadScene(scene);
    public void RestartLevel() => SceneManager.LoadScene(SceneManager.GetActiveScene().name);
}