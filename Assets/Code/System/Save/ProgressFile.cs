﻿[System.Serializable] public class ProgressFile
 {
    public bool[] level;
    public bool[] book;

    public ProgressFile(MemoryScript memory)
    {
        this.book = memory.book;
        this.level = memory.level;
    }
 }