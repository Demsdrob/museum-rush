using UnityEngine;

public class MemoryScript : MonoBehaviour
{
    #region Singelton

    public static MemoryScript memory;

    private void Awake()
    {
        if(memory == null)
        {
            DontDestroyOnLoad(this);
            memory = this;
            Load();
        }
        else
            Destroy(gameObject);
    }

    #endregion

    public bool[] level;
    public bool[] book;

    public void UnlockPage(int current) => book[current] = true;

    public void Save() => SaveLoadData.SaveProgress(this);
    public void Load()
    {
        ProgressFile file = SaveLoadData.LoadProgress();
        if (file != null)
        {
            level = file.level;
            book = file.book;
        }
    }
}