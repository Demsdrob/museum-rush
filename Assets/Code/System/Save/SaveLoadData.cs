using UnityEngine;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

[System.Serializable]
public static class SaveLoadData
{
    public static void SaveProgress(MemoryScript memory)
    {
        BinaryFormatter formatter = new BinaryFormatter();

        string path = Application.persistentDataPath + "/LevelProgress";
        FileStream stream = new FileStream(path, FileMode.Create);

        ProgressFile file = new ProgressFile(memory);

        formatter.Serialize(stream, file);
        stream.Close();
    }

    public static ProgressFile LoadProgress()
    {
        string path = Application.persistentDataPath + "/LevelProgress";
        Debug.Log(path);

        if (File.Exists(path))
        {
            BinaryFormatter formatter = new BinaryFormatter();
            FileStream stream = new FileStream(path, FileMode.Open);

            ProgressFile file = formatter.Deserialize(stream) as ProgressFile;
            stream.Close();
            Debug.Log("Save file succesfully loaded from: " + path);

            return file;
        } else
        {
            Debug.LogWarning("No save file found.");
            return null;
        } 
    }
}
