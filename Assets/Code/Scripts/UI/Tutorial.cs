using UnityEngine;
using UnityEngine.EventSystems;

public class Tutorial : MonoBehaviour
{
    [SerializeField] private EventSystem _system;
    [SerializeField] private GameObject _target;
    private static bool _active;

    private void Start()
    {
        if (!_active) return;
        SetTutorial();
    }
    public void SetTutorial()
    {
        _active = true;
        _system.SetSelectedGameObject(_target);
        Destroy(gameObject);
    }
}