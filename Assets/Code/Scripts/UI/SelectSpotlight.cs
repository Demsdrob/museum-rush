using UnityEngine;

public class SelectSpotlight : MonoBehaviour
{
    [SerializeField] private Transform _spotlight;
    [SerializeField] private Transform[] _targets;

    public void SetPlayer(int index)
    {
        _spotlight.position = _targets[index].position;
        SkinSelected.skin = index;
    }
}