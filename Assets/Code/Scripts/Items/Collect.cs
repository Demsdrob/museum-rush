using UnityEngine;
using UnityEngine.Events;

[RequireComponent(typeof(BoxCollider))]
public class Collect : MonoBehaviour, ICollectable
{
    [SerializeField] private BoxCollider _collider;
    [SerializeField] private UnityEvent _onCollect;

    private void Awake()
    {
        if (_collider == null) return;
        _collider.isTrigger = true;
    }

    public void OnCollect() => _onCollect.Invoke();

    private void OnDrawGizmos()
    {
        if (_collider == null) return;

        Vector3 pos = transform.position + _collider.center;

        Gizmos.color = new Vector4(0, 1, 0.2f, 0.3f);
        Gizmos.DrawCube(pos, _collider.size);
        Gizmos.DrawWireCube(pos, _collider.size);
    }
}