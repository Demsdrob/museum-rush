using Cinemachine;
using UnityEngine;

public class Shake : MonoBehaviour
{
    [SerializeField] private CinemachineVirtualCamera _camera;
    private CinemachineBasicMultiChannelPerlin _noise;
    public float Current { set => _noise.m_FrequencyGain = value; }

    private void Start() => _noise = _camera.GetCinemachineComponent<CinemachineBasicMultiChannelPerlin>();
    private void Update()
    {
        if (_noise.m_FrequencyGain == 0) return;
        if (_noise.m_FrequencyGain <= 0.01f) _noise.m_FrequencyGain = 0;

        _noise.m_FrequencyGain = Mathf.Lerp(_noise.m_FrequencyGain, 0, Time.deltaTime * 10);
    }
}