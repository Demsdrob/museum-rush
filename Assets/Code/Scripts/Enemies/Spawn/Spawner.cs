using System.Collections.Generic;
using System.Collections;
using UnityEngine;

namespace Enemy
{
    public class Spawner : MonoBehaviour
    {
        [System.Serializable] private struct Spawn
        {
            public Transform point;
            public GameObject prefab;
            [Range(0, 10)] public float delay;
            [Range(0, 10)] public float time;
        }
        [System.Serializable] private struct Pool
        {
            [Range(0, 10)] public int size;
            public Transform parent;
        }

        [SerializeField] private Spawn _spawn;
        [SerializeField] private Pool _pool;

        private Queue<Spawned> _poolList = new Queue<Spawned>();

        private IEnumerator Start()
        {
            for (int i = 0; i < _pool.size; i++)
            {
                Spawned instance = Instantiate(_spawn.prefab, _pool.parent).GetComponent<Spawned>();
                if (_spawn.time != 0) instance.SetTime(_spawn.time);
                _poolList.Enqueue(instance);
                instance.SetActive(false);
            }

            while (true)
            {
                yield return new WaitForSeconds(_spawn.delay);
                SpawnFromPool(_spawn.point.position, _spawn.point.rotation);
            }
        }
        public void SpawnFromPool(Vector3 position, Quaternion rotation)
        {
            Spawned obj = _poolList.Dequeue();

            obj?.SetSpawn();
            obj?.Position(position);
            obj?.Rotation(rotation);

            _poolList.Enqueue(obj);
        }
    }
}