using System.Collections;
using UnityEngine;

namespace Enemy
{
    public class Spawned : MonoBehaviour
    {
        [SerializeField] private Transform _self;
        [SerializeField, Range(0, 10)] private float _timeToDespawn = 1;

        public void SetActive(bool value) => gameObject.SetActive(value);
        public void Position(Vector3 position) => _self.position = position;
        public void Rotation(Quaternion rotation) => _self.rotation = rotation;
        public void SetTime(float time) => _timeToDespawn = time;
        public void SetSpawn()
        {
            gameObject.SetActive(true);
            StartCoroutine(Spawn());
        }
        
        private IEnumerator Spawn()
        {
            yield return new WaitForSeconds(_timeToDespawn);
            gameObject.SetActive(false);
        }
    }
}