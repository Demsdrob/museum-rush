using UnityEngine;

namespace Enemy
{
    [RequireComponent(typeof(Rigidbody))]
    public class RotateAxis : MonoBehaviour
    {
        [SerializeField] private Rigidbody _rgb;

        [SerializeField] private Vector3 _axis;
        [SerializeField, Range(0, 10)] private float _speed = 1;

        private void FixedUpdate()
        {
            Quaternion direction = Quaternion.Euler(_axis * _speed * 100 * Time.fixedDeltaTime);
            _rgb?.MoveRotation(_rgb.rotation * direction);
        }
    }
}