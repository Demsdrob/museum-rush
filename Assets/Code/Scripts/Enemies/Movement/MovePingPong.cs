using UnityEngine;

namespace Enemy
{
    [RequireComponent(typeof(Rigidbody))]
    public class MovePingPong : MonoBehaviour
    {
        [SerializeField] private Transform _self, _mesh;
        [SerializeField] private Rigidbody _rgb;
        [SerializeField] private Vector3 _direction;
        [SerializeField, Range(0, 20)] private float _distance = 1;
        [SerializeField, Range(0, 20)] private float _speed = 1;

        private Vector3 _start, _target;
        private float _current;
        private bool _back;

        private void Start() => _start = _self.position;
        private void Update()
        {
            _target = _start + _distance * _direction;
            if (!_back && Target(_self.position, _target)) _back = true;
            else if (_back && Target(_self.position, _start)) _back = false;

            if (_mesh == null) return;
            _mesh.LookAt(_self.position + (!_back ? _direction : -_direction));
        }
        private void FixedUpdate()
        {
            float velocity = _speed * Time.fixedDeltaTime * 10;
            _current += velocity * (!_back ? 1 : -1);
            _rgb.MovePosition(_current * _direction + _start);
        }
        private bool Target(Vector3 pos, Vector3 target)
        {
            Vector3 normal = (pos - target).normalized;
            Vector3 direction = _direction.normalized;
            return !_back ? normal == direction : normal == -direction;
        }
    }
}