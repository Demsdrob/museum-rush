using UnityEngine;

namespace Enemy
{
    [RequireComponent(typeof(Rigidbody))]
    public class MoveStraight : MonoBehaviour
    {
        [SerializeField] private Transform _self;
        [SerializeField] private Rigidbody _rgb;

        [SerializeField, Range(0, 10)] private float _speed;
        [SerializeField] private Vector3 _direction;

        private void FixedUpdate()
        {
            Vector3 direction = _self.TransformDirection(_direction);
            _rgb?.MovePosition(_rgb.position + direction * _speed * Time.fixedDeltaTime);
        }
    }
}