using UnityEngine;
using UnityEngine.Events;

[RequireComponent(typeof(BoxCollider))]
public class Trigger : MonoBehaviour
{
    [SerializeField] private BoxCollider _collider;
    [SerializeField] private string _tag = "Player";
    [SerializeField] private UnityEvent _onTriggerEnter;

    private void OnTriggerEnter(Collider other)
    {
        if (!other.CompareTag(_tag)) return;
        _onTriggerEnter.Invoke();
    }
    private void OnDrawGizmos()
    {
        if (_collider == null) return;

        Vector3 pos = transform.position + _collider.center;

        Gizmos.color = new Vector4(0, 0.8f, 1, 0.6f);
        Gizmos.DrawCube(pos, _collider.size);
        Gizmos.DrawWireCube(pos, _collider.size);
    }
}