using UnityEngine;
using UnityEngine.Events;

namespace Player
{
    public class Death : MonoBehaviour
    {
        [SerializeField] private UnityEvent _onInstantDeath;
        [SerializeField, Range(0, 5)] private float _delay = 1;
        [SerializeField] private UnityEvent _onDelayDeath;
        private bool _isDeath;

        private async void OnTriggerEnter(Collider other)
        {
            if (_isDeath) return;
            if (!other.CompareTag("Enemy")) return;

            _isDeath = true;
            _onInstantDeath.Invoke();

            float time = _delay * 1000;
            await System.Threading.Tasks.Task.Delay((int)time);
            _onDelayDeath.Invoke();
        }
    }
}