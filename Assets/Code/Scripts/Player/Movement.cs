using UnityEngine;
using UnityEngine.InputSystem;

namespace Player
{
    public class Movement : MonoBehaviour
    {
        [SerializeField] private Transform _self;
        [SerializeField] private Animator _animator;
        [SerializeField] private Transform _camera;
        [SerializeField] private Joystick _joistick;
        [Space]
        [SerializeField] private CharacterController _controller;
        [SerializeField] private Transform _mesh;
        [SerializeField, Range(1, 10)] private float _speed = 1;

        private Vector3 _camForward, _camRight;
        private Vector2 _input;
        private float _anim;

        private void Start()
        {
            _camForward = _camera.forward; _camForward.y = 0; _camForward.Normalize();
            _camRight = _camera.right; _camRight.y = 0; _camRight.Normalize();
        }
        private void Update()
        {
            if (_joistick != null) _input = _joistick.Direction;

            //move
            Vector3 direction = _camForward * _input.y + _camRight * _input.x;
            _controller.Move(direction * _speed * Time.deltaTime);
            HandleGravity();

            _anim = Mathf.Lerp(_anim, _input.magnitude, Time.deltaTime * 8);
            _animator.SetLayerWeight(1, _anim);

            //rotate
            if (_input == Vector2.zero) return;

            Quaternion rot = _mesh.rotation;
            Vector3 lookAt = new Vector3(direction.x, 0.0f, direction.z);
            _mesh.rotation = Quaternion.Slerp(rot, Quaternion.LookRotation(lookAt), Time.deltaTime * 10);
        }
        private void OnMove(InputValue value)
        {
            _input = value.Get<Vector2>();
        }
        private void HandleGravity()
        {
            if (_self.position.y != 0)
            {
                _controller.enabled = false;
                _self.position = new Vector3(_self.position.x, 0, _self.position.z);
                _controller.enabled = true;
            }
        }
        private void OnTriggerEnter(Collider other)
        {
            if (!other.CompareTag("Item")) return;

            other.GetComponent<ICollectable>().OnCollect();
            other.gameObject.SetActive(false);
        }
    }
}