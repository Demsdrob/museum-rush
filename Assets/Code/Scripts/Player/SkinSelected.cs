using UnityEngine;

public class SkinSelected : MonoBehaviour
{
    public static int skin;
    [SerializeField] private Transform _parent;
    [SerializeField] private GameObject[] _prefabs;

    private void Start()
    {
        if (_parent == null) return;

        int num = Mathf.Clamp(skin, 0, _prefabs.Length - 1);
        if(_prefabs[num] != null) Instantiate(_prefabs[num], _parent);
    }
}