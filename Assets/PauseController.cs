using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PauseController : MonoBehaviour
{
    [SerializeField] GameObject pauseMenu, ui, joy;
    [SerializeField] GameObject anchorActive, anchorInactive, joyInactive, joyActive;
    Coroutine coroutineA, coroutineB, coroutineC;
    private AudioSource source;

    private void Start()
    {
        GameObject audio = GameObject.FindGameObjectWithTag("AudioManager");
        if(audio != null) source = audio.GetComponent<AudioSource>();
    }
    public void Mute(bool value)
    {
        if (source == null) return;
        source.mute = value;
    }


    public void SetPause()
    {
        if (coroutineA != null) StopCoroutine(coroutineA);
        coroutineA = StartCoroutine(MovePausePanel(anchorActive.transform.position));

        if (coroutineB != null) StopCoroutine(coroutineB);
        coroutineB = StartCoroutine(MoveUIPanel(anchorInactive.transform.position));

        if (coroutineC != null) StopCoroutine(coroutineC);
        coroutineC = StartCoroutine(MoveJoyPanel(joyInactive.transform.position)); 

        Time.timeScale = 0;
        Debug.Log("paused");
    }
    public void Resume()
    {
        if (coroutineA != null) StopCoroutine(coroutineA);
        coroutineA = StartCoroutine(MovePausePanel(anchorInactive.transform.position));
        
        if (coroutineB != null) StopCoroutine(coroutineB);
        coroutineB = StartCoroutine(MoveUIPanel(anchorActive.transform.position));
        
        if (coroutineC != null) StopCoroutine(coroutineC);
        coroutineC = StartCoroutine(MoveJoyPanel(joyActive.transform.position));
        Time.timeScale = 1;
    }

    IEnumerator MoveUIPanel(Vector2 anchor)
    {
        while (Vector2.Distance(anchor , ui.transform.position) > 1f)
        {
            Debug.Log(Vector2.Distance(anchor, ui.transform.position));

            ui.transform.position = Vector2.Lerp(ui.transform.position, anchor, Time.unscaledDeltaTime * 4f);
            yield return null;
        }
    }
    IEnumerator MoveJoyPanel(Vector2 anchor)
    {
        while (Vector2.Distance(anchor , joy.transform.position) > 1f)
        {
            Debug.Log(Vector2.Distance(anchor, joy.transform.position));

            joy.transform.position = Vector2.Lerp(joy.transform.position, anchor, Time.unscaledDeltaTime * 4f);
            yield return null;
        }
    }
    IEnumerator MovePausePanel(Vector2 anchor)
    {
        while (Vector2.Distance(anchor , pauseMenu.transform.position) > 1f)
        {
            Debug.Log(Vector2.Distance(anchor, pauseMenu.transform.position));

            pauseMenu.transform.position = Vector2.Lerp(pauseMenu.transform.position, anchor, Time.unscaledDeltaTime * 4f);
            yield return null;
        }
    }
}
