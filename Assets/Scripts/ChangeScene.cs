using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ChangeScene : MonoBehaviour
{
    public void Changescene(string name)
    {
        Time.timeScale = 1;
        SceneManager.LoadScene(name);

    }

    public void SetLevel(int level)
    {
        //GameObject.Find("Memory").GetComponent<MemoryScript>().currentLevel = level - 1;
    }

    public void Quit ()
    {
        Application.Quit();
    }
}
