using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnScript : MonoBehaviour
{
    public GameObject enemy;
    public float timer;

    private void Start() => InvokeRepeating("EnemySpawner", timer, timer);
    private void EnemySpawner()
    {
        GameObject obj = Instantiate(enemy);
        obj.transform.position = gameObject.transform.position;
    }
}