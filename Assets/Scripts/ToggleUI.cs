using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ToggleUI : MonoBehaviour
{
    [SerializeField] private Sprite enable, disable;
    private Image img;

    private void Awake() => img = GetComponent<Image>();

    public void Swipe(bool enable)
    {
        img.sprite = enable ? this.enable : disable;
    }
}