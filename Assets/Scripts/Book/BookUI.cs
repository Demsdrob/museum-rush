using UnityEngine;

public class BookUI : MonoBehaviour
{
    [SerializeField] private GameObject[] _pages;
    private int _current;

    public void MovePage(int direction)
    {
        _current = Mathf.Clamp(_current += direction, 0, _pages.Length - 1);
        SetCurrentPage(_current);
    }
    public void SetCurrentPage(int index)
    {
        _current = index;
        foreach (var item in _pages) { if (!item.activeSelf) continue; item.SetActive(false); }
        _pages[index].SetActive(true);
    }
}