using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BookControl : MonoBehaviour
{
    public MemoryScript memoryS;
    public Button[] pageSelector;
    public GameObject[] page;
    public GameObject pageUI;
    public GameObject bookUI;
    public int currentPage;
    public Text pageNumber;
    
    void Start()
    {
        memoryS = GameObject.Find("Memory").GetComponent<MemoryScript>();
        if (memoryS != null)
        {
            for (int i = 0; i < memoryS.book.Length; i++)
            {
                pageSelector[i].interactable = memoryS.book[i];

                if (!pageSelector[i].interactable)
                {
                    Text[] texts = page[i].GetComponentsInChildren<Text>(true);
                    for (int j = 0; j < texts.Length; j++)
                    {
                        texts[j].text = " ??? ";
                    }
                }
            }
        }
        //Unlock all completed levels.
    }

    public void UpdatePageNumber()
    {
        int current = currentPage + 1;
        pageNumber.text = (current) + " / 10";
    }
}
