using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PageButtons : MonoBehaviour
{
    public BookControl control;
    public void DeselectPage()
    {
        control.page[control.currentPage].SetActive(false);
        Debug.Log("Deactivating page:" + control.currentPage);
        for (int i = 0; i < control.pageSelector.Length; i++)
        {
            control.pageSelector[i].interactable = control.memoryS.book[i];
        }
        control.pageUI.SetActive(false);
        control.bookUI.SetActive(false);
    }
    public void NextPage()
    {
        if (control.currentPage == 9)
        {
            control.page[0].SetActive(true);
            control.page[9].SetActive(false);
            control.currentPage = 0;
        }
        else
        {
            control.page[control.currentPage + 1].SetActive(true);
            control.page[control.currentPage].SetActive(false);
            control.currentPage += 1;
        }
        control.UpdatePageNumber();
    }
    public void PreviousPage()
    {
        if (control.currentPage == 0)
        {
            control.page[9].SetActive(true);
            control.page[0].SetActive(false);
            control.currentPage = 9;
        }
        else
        {
            control.page[control.currentPage - 1].SetActive(true);
            control.page[control.currentPage].SetActive(false);
            control.currentPage -= 1;
        }
        control.UpdatePageNumber();
    }
}
