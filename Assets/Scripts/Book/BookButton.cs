using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class BookButton : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    public BookControl control;
    public Camera mainCam;

    [SerializeField]
    private RectTransform pointer;

    private void Start()
    {
        //control = GameObject.Find("Book").GetComponent<BookControl>();
        if (pointer == null)
        {
            pointer = GetComponentsInChildren<RectTransform>()[1];
            //pointer.gameObject.SetActive(false);
        }
    }
    private void Update()
    {
        if (pointer)
        {
            //if (pointer.gameObject.activeInHierarchy)
            //{
            //    //pointer.transform.position = new Vector3(mainCam.ScreenToWorldPoint(Input.mousePosition).x, mainCam.ScreenToWorldPoint(Input.mousePosition).y,0f);
            //    //Debug.Log(mainCam.ScreenToWorldPoint(Input.mousePosition).x + " " + mainCam.ScreenToWorldPoint(Input.mousePosition).y + " " + mainCam.ScreenToWorldPoint(Input.mousePosition).z);
            //    var mousePos = Input.mousePosition;
            //    mousePos.z = 100; // select distance = 10 units from the camera
            //    var camPos = mainCam.ScreenToWorldPoint(mousePos);
            //    camPos.y += 1f;
            //    pointer.transform.position = camPos;
            //    Debug.Log(mainCam.ScreenToWorldPoint(mousePos));
            //}
        }
    }
    public void OnPointerEnter(PointerEventData eventData)
    {
        if (GetComponent<Button>().interactable)
        {
            //pointer.gameObject.SetActive(true);
        }
    }
    public void OnPointerExit(PointerEventData eventData)
    {
        if (GetComponent<Button>().interactable)
        {
            //pointer.gameObject.SetActive(false);
        }
    }
    public void SelectPage(int buttonInput)
    {
        //pointer.gameObject.SetActive(false);
        control.currentPage = buttonInput;
        control.page[control.currentPage].SetActive(true);
        Debug.Log("Activating page:" + control.currentPage);
        control.pageUI.SetActive(true);
        control.bookUI.SetActive(true);
        control.UpdatePageNumber();
        for (int i = 0; i < control.pageSelector.Length; i++)
        {
            control.pageSelector[i].interactable = false;
        }
    } //TODO Set all pages, button Numbers, check pages
}
