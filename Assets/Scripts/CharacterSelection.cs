using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CharacterSelection : MonoBehaviour
{
    MemoryScript memoryS;
    [SerializeField] Light[] spot;
    public Button exitButton;
    public bool[] spotOn;

    private void Start()
    {
        memoryS = GameObject.Find("Memory").GetComponent<MemoryScript>();
        

    }

    private void LateUpdate()
    {
        if (exitButton.interactable)
        {
            //spot[0].enabled = !memoryS.skin || spotOn[0];
            //spot[1].enabled = memoryS.skin || spotOn[1];
        } else
        {
            spot[0].enabled = spotOn[0];
            spot[1].enabled = spotOn[1];
        }
                          
    }

    public void SetSkin(bool value)
    {
        //memoryS.skin = value;
        //exitButton.interactable = true;
    }
}
