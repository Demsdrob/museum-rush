using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class ButtonCollision : MonoBehaviour , IPointerEnterHandler, IPointerExitHandler
{
    private CharacterSelection characterSelection;
    public int spotNumber;
    private void Start()
    {
        characterSelection = GameObject.Find("SelectorManager").GetComponent<CharacterSelection>();
    }
    public void OnPointerEnter(PointerEventData eventData)
    {
        characterSelection.spotOn[spotNumber] = true;
    }
    public void OnPointerExit(PointerEventData eventData)
    {
        characterSelection.spotOn[spotNumber] = false;
    }
}
