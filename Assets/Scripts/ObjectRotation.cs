using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectRotation : MonoBehaviour
{
    public bool world;
    public float rotX;
    public float rotY;
    public float rotZ;
    public float loopSpd;

    void Start()
    {
        
    }

    void FixedUpdate()
    {
        if (world)
        {
            transform.Rotate(new Vector3(rotX, rotY, rotZ) * loopSpd, Space.World);
        }
        else
        {
            transform.Rotate(new Vector3(rotX, rotY, rotZ) * loopSpd, Space.Self);
        }
    }
}
