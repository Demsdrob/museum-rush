using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelControl : MonoBehaviour
{
    public MemoryScript memoryS;
    public Button[] LvlSelector;
    // Start is called before the first frame update
    void Start()
    {
        memoryS = GameObject.Find("Memory").GetComponent<MemoryScript>();
        if (memoryS != null)
        {
            for (int i = 0; i < memoryS.level.Length; i++)
            {
                LvlSelector[i].interactable = memoryS.level[i];
                if (memoryS.level[i])
                {
                    LvlSelector[i].GetComponentInChildren<Text>().color = new Color(221, 207, 204, 1);
                }
            }
        }
    }
}
