using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyScript : MonoBehaviour
{
    [SerializeField, Range(-1, 1)] private int _direction;
    [SerializeField] private float _speed; 
    [SerializeField] private float _limit;
    private Transform _self;
    private Transform _child;
    private Vector3 startPos;
    private float multiply = 1;

    public Rigidbody rb;
    //public float speed;
    //public bool direction;

    private void Awake() => _self = transform;
    private void Start()
    {
        multiply = _direction;
        startPos = _self.position;
        _child = _self.GetChild(0);
    }
    private void Update()
    {
        float limit = _limit * 100;
        float amount = limit * Mathf.Abs(_direction);

        if (_self.position.z > startPos.z + amount)
        {
            multiply = -1;
            _child.forward = Vector3.forward * multiply;
        }
        if (_self.position.z < startPos.z - amount)
        {
            multiply = 1;
            _child.forward = Vector3.forward * multiply;
        }
    }
    private void FixedUpdate()
    {
        rb?.MovePosition(rb.position + (multiply * _speed * 10 * Vector3.forward));


        //if (direction)
        //{
        //    //verdadero = horizontal, falso = vertical
        //    rb.velocity = new Vector3(speed, 0, 0);
        //}
        //else
        //{
        //    rb.velocity = new Vector3(0, 0, speed);
        //}
    }

    private void OnCollisionEnter(Collision collision)
    {
        //if (collision.gameObject.CompareTag("Wall"))
        //{
        //    transform.Rotate(new Vector3(0, 180, 0));
        //    speed *= -1;
        //}

        if (collision.gameObject.CompareTag("Wall2"))
        {
            Destroy(gameObject);
        }

        /*if (collision.gameObject.CompareTag("Player"))
        {
            Destroy(collision.gameObject);
        }*/
    }
}
