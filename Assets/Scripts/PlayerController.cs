using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Animations;

public class PlayerController : MonoBehaviour
{
    private CharacterController movement;
    private MemoryScript memoryS;
    [SerializeField] private Animator anim;
    private ChangeScene sceneChanger;
    [SerializeField] private Transform model;
    [SerializeField] private string currentScene;
    [SerializeField] private string nextScene;
    [SerializeField] private Controls input;

    public float speed;
    private bool key;
    private bool item;
    public int lvl;
    public float timer;
    public bool death;
    public GameObject arrow;

    public CameraShake cameraShake;

    private Vector3 _start;
    private float _currentSpeed;
    private MeshRenderer _render;
    private BoxCollider _collider;

    private void Awake()
    {
        sceneChanger = GetComponent<ChangeScene>();
        movement = GetComponent<CharacterController>();
        _render = GetComponent<MeshRenderer>();
        _collider = GetComponent<BoxCollider>();
        input = new Controls();
    }

    private void OnEnable()
    {
        input.Enable();
    }
    private void OnDisable()
    {
        input.Disable();
    }
    void Start()
    {
        _currentSpeed = speed;
        _start = transform.position;
        memoryS = GameObject.Find("Memory").GetComponent<MemoryScript>();
        //if (memoryS.skin)
        //{
        //    model = Instantiate(memoryS.skins[1], transform).transform;
        //} else
        //{
        //    model = Instantiate(memoryS.skins[0], transform).transform;
        //}
        anim = GetComponentInChildren<Animator>();
        StartCoroutine(Death());
    }

    private void Update()
    {
        if (key) arrow.SetActive(true);
    }

    private void FixedUpdate()
    {
        Vector2 axis = input.Game.Move.ReadValue<Vector2>(); 
        Vector3 moveDir = new Vector3(axis.x * _currentSpeed, 0, axis.y * _currentSpeed);
        //Debug.Log(moveDir);
        anim.SetFloat("X", axis.x);
        anim.SetFloat("Y", axis.y);
        anim.SetFloat("speed", moveDir.normalized.magnitude);

        if(moveDir != Vector3.zero)
            model.localRotation = Quaternion.LookRotation(moveDir.normalized);

        moveDir.y = !movement.isGrounded ? moveDir.y -= 4f : 0f;
        movement.Move(moveDir);
    }
    private IEnumerator Death()
    {
        yield return new WaitUntil(() => death);
        yield return new WaitForSeconds(timer);
        SceneManager.LoadScene(currentScene);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Enemy"))
        {
            StartCoroutine(cameraShake.Shake(.15f, .4f));
            death = true;
            _currentSpeed = 0;
            _render.enabled = _collider.enabled = false;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Goal"))
        {
            if (key)
            {
                //memoryS.UnlockNextLevel();
                if (item)
                {
                    memoryS.UnlockPage(1);
                }
                //memoryS.currentLevel += 1;
                memoryS.Save();
                sceneChanger.Changescene(nextScene);
            } else
            {
                //TODO Add splash text
                Debug.Log("KeyMissing");
            }
        }

        if (other.gameObject.CompareTag("Key"))
        {
            key = true;
            Destroy(other.gameObject);
        }
        
        if (other.gameObject.CompareTag("Item"))
        {
            item = true;
            Destroy(other.gameObject);
        }
    }

    //private void OnCollisionStay(Collision collision)
    //{
    //    Debug.Log(collision.gameObject.tag);
    //}

    //private void Death()
    //{
    //    timer -= Time.deltaTime;

    //    if (timer <= 0f)
    //    {
    //        SceneManager.LoadScene(currentScene);
    //    }
    //}
}
