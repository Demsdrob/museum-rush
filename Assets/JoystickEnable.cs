using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.InputSystem.Layouts;
using UnityEngine.InputSystem.OnScreen;

public class JoystickEnable : OnScreenControl, IPointerDownHandler,IPointerUpHandler, IDragHandler
{
    [InputControl(layout = "Vector2")]
    [SerializeField]
    private string m_ControlPath;
    [SerializeField] 
    private Image joystickIn, joystickOut;
    [SerializeField]
    Vector2 initPosition;
    private float alpha = 0.5f;
    public float movementRange = 50;
    private void Start()
    {
        joystickOut.color = new Color(255, 255, 255, alpha);
        joystickIn.color = new Color(255, 255, 255, alpha);
    }
    public void OnPointerDown(PointerEventData data)
    {
        StopCoroutine("FadeOut");
        alpha = 1f;
        joystickOut.transform.position = data.position;
        initPosition = data.position;
        joystickOut.color = new Color(255, 255, 255, alpha);
        joystickIn.color = new Color(255, 255, 255, alpha);
    }
    public void OnDrag(PointerEventData data)
    {
        var delta = Vector2.ClampMagnitude(data.position - initPosition, movementRange);
        joystickIn.transform.position = delta + initPosition;
        var newPos = new Vector2(delta.x / movementRange, delta.y / movementRange);
        SendValueToControl(newPos);
    }
    public void OnPointerUp(PointerEventData data)
    {
        joystickIn.transform.position = joystickOut.transform.position;
        SendValueToControl(Vector2.zero);
        StartCoroutine("FadeOut");
    }
    protected override string controlPathInternal
    {
        get => m_ControlPath;
        set => m_ControlPath = value;
    }
    IEnumerator FadeOut()
    {
        while (alpha > 0.5f)
        {
            alpha -= 0.02f;
            joystickOut.color = new Color(255, 255, 255, alpha);
            joystickIn.color = new Color(255, 255, 255, alpha);
            yield return null;
        }
    }
}
